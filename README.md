# README #

This is a finished project in which I have used many different Machine Learning and Statistical Learning techniques. 

### What is this repository for? ###

* This repository contains files that I have created during the project. There is Python code used to solve the task, pdf version of the report and short PowerPoint presentation about the solution. 
* The project was initiated by myself and done in my free time.
* Version 1.0

### What to look for? ###

* Objective was to get more experience in building different models and not to use a huge number of estimators inside each one.
* Dataset is taken from https://www.kaggle.com/c/bike-sharing-demand
* Python 2.7
* Libraries used: pylab, calendar, numpy, pandas, seaborn, scipy, datetime, matplotlib, warnings, math, sklearn, ephem, statsmodels, csv.

### Models built ###

* Regression Models - Multiple Linear Regression, Gradient Boosting Regression, Random Forrest Regression, Random Forrest + Gradient Boosting Regressions, PCA + Gradient Boosting Regressions.
* Time series models - Baseline model, FFT Extrapolation, ARIMA, ARIMA with exogeneus variables.

### Contact me ###

* email: pietruszka@agh.edu.pl
* LinkedIn: https://www.linkedin.com/in/pietruszka-phd/
* BitBucket: https://bitbucket.org/pietruszkaM/

### Thank you for watching! ###